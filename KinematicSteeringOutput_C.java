
package Movement;

public class KinematicSteeringOutput extends KinematicOutput
{

    public KinematicSteeringOutput() {
    }

    public KinematicSteeringOutput(Vector2D velocity, float rotation) {
        super(velocity, rotation);
    }
    
}
